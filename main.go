package main

import (
	"github.com/gin-gonic/gin"

	"example/work1/models"

	"example/work1/controllers"
)

func main() {
	r := gin.Default()

	models.ConnectDataBase()

	r.GET("/", controllers.ShowHome)

	r.GET("/books", controllers.FindBooks)
	r.GET("/books/:id", controllers.FindBook)
	r.POST("/books", controllers.CreateBook)
	r.PATCH("/books/:id", controllers.UpdateBook)
	r.DELETE("/books/:id", controllers.DeleteBook)

	r.GET("/users", controllers.FindUsers)
	r.GET("/users/:id", controllers.FindUser)
	r.POST("/users", controllers.CreateUser)
	r.PATCH("/users/:id", controllers.UpdateUser)
	r.DELETE("/users/:id", controllers.DeleteUser)

	r.Run() // listen and serve on 0.0.0.0:8080
}
