package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name  string `json:"name"`
	Books []Book `gorm:"foreignkey:user_id" json:"books"`
}
