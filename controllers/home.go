package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /
// Show home page
func ShowHome(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "hello world"})
}
