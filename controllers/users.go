package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"example/work1/models"
)

// GET /users
// Get all users
func FindUsers(c *gin.Context) {
	var users []models.User

	if result := models.DB.Find(&users); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": users})
}
