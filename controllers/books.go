package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"example/work1/models"
)

// GET /books
// Get all books
func FindBooks(c *gin.Context) {
	var books []models.Book

	if result := models.DB.Find(&books); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": books})
}
