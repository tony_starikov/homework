## About

The first homework for eliftech. Rest api for crud users and books.

## How to use

Run in terminal: go run main.go

Check http://localhost:8080/ in browser or postman, or insomnia and get greating for you

GET http://localhost:8080/users - to get all users

GET http://localhost:8080/users/:id - to get user by id

POST http://localhost:8080/users - to add a new user

PATCH http://localhost:8080/users/:id - to update user by id

DELETE http://localhost:8080/users/:id - to update user by id

And almost the same routes and methods for books

Enjoy)